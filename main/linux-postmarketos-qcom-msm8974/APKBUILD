_flavor=postmarketos-qcom-msm8974
_config="config-$_flavor.$CARCH"

pkgname=linux-$_flavor

pkgver=5.4.0_rc6
pkgrel=1

arch="armv7"
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8974 devices"
url="https://kernel.org/"
makedepends="dtbtool perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev openssl-dev file bison flex"
options="!strip !check !tracedeps"
_commit="f5cb192a3e49fc69c2c1df8b684f954152a8a80c"
source="
	linux-postmarketos-qcom-$_commit.tar.gz::https://gitlab.com/postmarketOS/linux-postmarketos/-/archive/qcom/linux-postmarketos-qcom-$_commit.tar.gz
	config-$_flavor.armv7
"

subpackages="$pkgname-dev"

license="GPL2"
_abi_release=$_pkgver
_carch=$CARCH
case "$_carch" in
aarch64*) _carch="arm64" ;;
arm*) _carch="arm" ;;
ppc*) _carch="powerpc" ;;
s390*) _carch="s390" ;;
esac

HOSTCC="${CC:-gcc}"
HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

builddir="$srcdir/linux-postmarketos-qcom-$_commit"

prepare() {
	local _patch_failed=
	ls "$srcdir"
	# first apply patches in specified order
	for i in $source; do
		case $i in
		*.patch|*.patch::*)
			_patch=${i%::*}
			msg "Applying $_patch..."
			if ! patch -s -p1 -N -i "$srcdir"/$_patch; then
				echo $_patch >>failed
				_patch_failed=1
			fi
			;;
		esac
	done

	if ! [ -z "$_patch_failed" ]; then
		error "The following patches failed:"
		cat failed
		return 1
	fi

	mkdir -p "$srcdir"/build
	cp -v "$srcdir"/$_config "$srcdir"/build/.config
	make -C "$builddir" O="$srcdir"/build ARCH="$_carch" HOSTCC="$HOSTCC" \
		olddefconfig
}

build() {
	cd "$srcdir"/build
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor" \
		CFLAGS_MODULE=-fno-pic

	dtbTool -s 2048 -p scripts/dtc/ -o arch/arm/boot/dt.img arch/arm/boot/dts --version=2
}

package() {
	cd "$srcdir/build/arch/$_carch/boot"

	if [ "$CARCH" == "aarch64" ]; then
		install -Dm644 "$srcdir/build/arch/$_carch/boot/Image" \
			"$pkgdir/boot/vmlinuz-$_flavor"
	else
		install -Dm644 "$srcdir/build/arch/$_carch/boot/"*zImage \
			"$pkgdir/boot/vmlinuz-$_flavor"
	fi

	install -D "$srcdir/build/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	install -Dm644 "$srcdir/build/arch/arm/boot/dt.img" \
		"$pkgdir/boot/dt.img"

	cd "$srcdir"/build

	local _install
	case "$CARCH" in
	aarch64*|arm*)	_install="modules_install dtbs_install" ;;
	*)		_install="modules_install" ;;
	esac

	make -j1 $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}


dev() {
	# copy the only the parts that we really need for build 3rd party
	# kernel modules and install those as /usr/src/linux-headers,
	# similar to what ubuntu does
	#
	# this way you don't need to install the 300-400 kernel sources to
	# build a tiny kernel module
	#
	pkgdesc="Headers and script for third party modules for postmarketos kernel"
	depends="gmp-dev bash perl"
	local dir="$subpkgdir"/usr/src/linux-headers-$_abi_release

	# first we import config, run prepare to set up for building
	# external modules, and create the scripts
	mkdir -p "$dir"
	cp "$srcdir"/$_config "$dir"/.config
	make -j1 -C "$builddir" O="$dir" ARCH="$_carch" HOSTCC="$HOSTCC" \
		olddefconfig prepare modules_prepare scripts

	# needed for 3rd party modules
	# https://bugzilla.kernel.org/show_bug.cgi?id=11143
	case "$CARCH" in
	ppc*) (cd "$dir" && make arch/powerpc/lib/crtsavres.o);;
	esac

	# remove the stuff that points to real sources. we want 3rd party
	# modules to believe this is the sources
	rm "$dir"/Makefile "$dir"/source

	# copy the needed stuff from real sources
	#
	# this is taken from ubuntu kernel build script
	# http://kernel.ubuntu.com/git/ubuntu/ubuntu-zesty.git/tree/debian/rules.d/3-binary-indep.mk

	cd "$builddir"
	find . -path './include/*' -prune \
		-o -path './scripts/*' -prune -o -type f \
		\( -name 'Makefile*' -o -name 'Kconfig*' -o -name 'Kbuild*' -o \
		   -name '*.sh' -o -name '*.pl' -o -name '*.lds' \) \
		-print | cpio -pdm "$dir"
	cp -a scripts include "$dir"
	find $(find arch -name include -type d -print) -type f \
		| cpio -pdm "$dir"

	install -Dm644 "$srcdir"/build/Module.symvers \
		"$dir"/Module.symvers

	mkdir -p "$subpkgdir"/lib/modules/$_abi_release
	ln -sf /usr/src/linux-headers-$_abi_release \
		"$subpkgdir"/lib/modules/$_abi_release/build
}

sha512sums="36c6b2fd37f5ae13937ab0d3900cc8ddee2c78f963d74198488264ce2b10916e8150b6fb9983fe3ef92ac5ffd6d3af290a027e51f4aa7fe9bb6677e11406de3d  linux-postmarketos-qcom-f5cb192a3e49fc69c2c1df8b684f954152a8a80c.tar.gz
6e51ab9de1881718d4518d93c143f99bada46ce0a6d543296587483eab63d0cd2b52ca7f3a0b407ce1256006ce8231655c0b907aaf1bff05c18ef0ac0cbc29fb  config-postmarketos-qcom-msm8974.armv7"
